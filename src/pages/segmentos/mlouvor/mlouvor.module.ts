import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MlouvorPage } from './mlouvor';

@NgModule({
  declarations: [
    MlouvorPage,
  ],
  imports: [
    IonicPageModule.forChild(MlouvorPage),
  ],
})
export class MlouvorPageModule {}
