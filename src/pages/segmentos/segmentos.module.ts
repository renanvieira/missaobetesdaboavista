import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { mSegmentos } from './segmentos';
import { MmembrosPage } from './mmembros/mmembros';

@NgModule({
  declarations: [
    mSegmentos
  ],
  imports: [
    IonicPageModule.forChild(mSegmentos)
    
  ],
})
export class mSegmentosModule {


}
