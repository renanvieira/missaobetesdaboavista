import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MmembrosPage } from './mmembros';

@NgModule({
  declarations: [
    MmembrosPage,
  ],
  imports: [
    IonicPageModule.forChild(MmembrosPage),
  ],
})
export class MmembrosPageModule {}
