import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DbjsonProvider } from '../../../providers/dbjson/dbjson';
import { mSegmentosModule } from '../segmentos.module';
/**
 * Generated class for the MmembrosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mmembros',
  templateUrl: 'mmembros.html',
  providers:[
    DbjsonProvider
  ]
})
export class MmembrosPage {
  //Declarando variável
  public membro = this.navParams.data;
  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public jsondb: DbjsonProvider,
  ) {}


  

  ionViewDidLoad() {
    console.log('ionViewDidLoad MmembrosPage');
    

    console.dir(this.membro.nome)
    

    //buscar os dados membros
    this.jsondb.getMembros().subscribe(function(data){
      //transforma parametro data em qualquer tipo(any)
      const response = (data as any);
      //transforma o resultado que vem como texto json em array
      const return_object = JSON.parse(response._body);
      let esse = return_object;
      // if(esse[0].id == 1){
      //   console.log("verdadeiro")
      // }
      // console.log(this.navParams.get('x'))
    })

  }

  dismiss(){
    this.viewCtrl.dismiss();
  }
}
