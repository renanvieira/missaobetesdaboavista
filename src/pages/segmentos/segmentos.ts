import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DbjsonProvider } from '../../providers/dbjson/dbjson';
import { HomePage } from '../home/home';
import { MmembrosPage } from './mmembros/mmembros';
import { MlouvorPage } from './mlouvor/mlouvor';



/**
 * Generated class for the MlouvorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-msegmentos',
  templateUrl: 'segmentos.html',
  //Declaração dos Providers que irá ser utilizado dentro da página.
  providers: [
    DbjsonProvider
  ]

})

export class mSegmentos {
  //Faz página segmentos já iniciar com notícias ativa.
  ministeriodelouvor: string = "noticias";
  //Declarando váriavel para colocar o valor da string lá em baixo
  public lista_membros = new Array<any>();
  //Declarando variável para colocar o requisitar dados como array
  public lista_mnoticias = new Array<any>();
  
  


  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public jsondb: DbjsonProvider,
    public modalCtrl: ModalController
  ) { }

  


  ionViewDidLoad() {
    console.log('ionViewDidLoad msegmentos');
    
    

    //Busca os dados do arquivo Json em string e transforma em array.
    this.jsondb.getMembros().subscribe(
      // Função com parâmetro data para retorna o que tem dentro do arquivo json.
      data => {
        //transforma o param data em any pois o subscribe pede que ele seja observable
        const response = (data as any);
        //transforma o arquivo json em array
        const return_object = JSON.parse(response._body);
        //Aplica o Return Object para o lista_membros
        this.lista_membros = return_object;
        console.log(return_object);
        
        
      }
    )

    this.jsondb.getNoticias().subscribe(
      data=> {
        const response = (data as any);
        const return_object = JSON.parse(response._body);
        this.lista_mnoticias = return_object;
      }
    )

  }

  membrosModal(x){
    let profileModal = this.modalCtrl.create(MmembrosPage, x);
    profileModal.present();

  }

  noticiaModal(x){
    let profileModal = this.modalCtrl.create(MlouvorPage)
    profileModal.present();
  }


 

}
