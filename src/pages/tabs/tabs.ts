import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { mSegmentos } from '../segmentos/segmentos';
import { MmembrosPage } from '../segmentos/mmembros/mmembros';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = MmembrosPage;
  tab5Root = mSegmentos;

  constructor() {

  }
}
