import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the DbjsonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbjsonProvider {

  constructor(
    public http: Http
  ) {
    console.log('Hello DbjsonProvider Provider');
  }

  private BaseUrl = 'assets/dbjson/';
  getMembros(){
    let arquivo = "membros.json"
    return this.http.get(this.BaseUrl + arquivo);
  };
  getNoticias(){
    let arquivo = "mnoticias.json";
    return this.http.get(this.BaseUrl+arquivo);
  };

}
